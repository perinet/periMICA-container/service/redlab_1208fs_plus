module main

go 1.21.1

require (
	gitlab.com/perinet/generic/lib/httpserver v0.0.0-20230721113448-51dceb287282
	gitlab.com/perinet/generic/lib/utils v0.0.0-20230628130910-2f4300736d64
	gitlab.com/perinet/periMICA-container/apiservice/redlab v0.0.3
	gitlab.com/perinet/periMICA-container/apiservice/security v0.0.0-20230428094054-1af9a40cfc53
)

require (
	github.com/alexandrevicenzi/go-sse v1.6.0 // indirect
	github.com/felixge/httpsnoop v1.0.1 // indirect
	github.com/google/uuid v1.3.0 // indirect
	github.com/gorilla/handlers v1.5.1 // indirect
	github.com/gorilla/mux v1.8.0 // indirect
	github.com/grantae/certinfo v0.0.0-20170412194111-59d56a35515b // indirect
	github.com/sstallion/go-hid v0.14.1 // indirect
	gitlab.com/perinet/generic/lib/redlab v0.0.5 // indirect
	golang.org/x/exp v0.0.0-20230510235704-dd950f8aeaea // indirect
	golang.org/x/sys v0.8.0 // indirect
)
