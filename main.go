/*
 * Copyright (c) 2018-2023 Perinet GmbH
 * All rights reserved
 *
 * This software is dual-licensed: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3 as
 * published by the Free Software Foundation. For the terms of this
 * license, see http://www.fsf.org/licensing/licenses/agpl-3.0.html,
 * or contact us at https://server.io/contact/ when you want license
 * this software under a commercial license.
 */

package main

import (
	"log"

	apiservice_redlab_1208fs_plus "gitlab.com/perinet/periMICA-container/apiservice/redlab/redlab_1208fs_plus"

	httpserver "gitlab.com/perinet/generic/lib/httpserver"
	auth "gitlab.com/perinet/generic/lib/httpserver/auth"
	webhelper "gitlab.com/perinet/generic/lib/utils/webhelper"
	security "gitlab.com/perinet/periMICA-container/apiservice/security"
)

var (
	logger         log.Logger = *log.Default()
	securityConfig security.SecurityConfig
)

func init() {
	log.SetPrefix("redlab service: ")
}

func main() {
	logger.Println("starting")
	httpserver.AddPaths(apiservice_redlab_1208fs_plus.PathsGet())

	auth.SetAuthMethod(securityConfig.ClientAuthMethod)

	httpserver.SetCertificates(httpserver.Certificates{
		CaCert:      webhelper.InternalVarsGet(security.Security_TrustAnchor_Get, map[string]string{"trust_anchor": "root_ca_cert"}),
		HostCert:    webhelper.InternalVarsGet(security.Security_Certificate_Get, map[string]string{"certificate": "host_cert"}),
		HostCertKey: webhelper.InternalVarsGet(security.Security_Certificate_Key_Get, map[string]string{"certificate": "host_cert"}),
	})

	httpserver.ListenAndServe("[::]:443")
}
